import { getTodoList, postMethod, deleteMethod, putMethod, } from "./async.js";

const arrayOfListItems = [];
let idNumber = 1;

let ListItem = function (title) {
  this.title = title;
}

//funkcja która dodaje nową pozycję na serwerze
function addItemToTheServer() {
  let newLi = addListItem();
  let liTitle = newLi.title;
  postMethod(liTitle);
}

function handleEvents() {
  // main add button event
  const list = document.getElementById('list');
  const addButton = document.getElementById('addBtn');

  addButton.addEventListener('click', addItemToTheServer);

  // handle ol button events
  list.addEventListener('click', function (event) {
    let id = event.target.parentElement.dataset.id;
    if (event.target.className === 'delete') { // usuwanie elementu
      deleteMethod(id);
      event.target.parentElement.classList.add('changeOpacity');
      setTimeout(function () {
        event.target.parentElement.remove();
      }, 1000);
    } else if (event.target.className === 'edit') { // edycja elementu
      let title = prompt('Edit your task');
      putMethod(title,id);
      event.target.parentElement.replaceChild(document.createTextNode(title), event.target.parentElement.firstChild);
    } else if (event.target.className === 'done') {
      event.target.parentElement.style.textDecoration = 'line-through';
    }
  })
}

// funkcja dodająca nowe li
function addListItem() {
  let newItem = takeInputData();
  updateUI(newItem);
  return newItem;
}

// dodanie elementu do bazy danych w kodzie
function createNewListItem(title) {
  // wywołanie konstruktora ListItem, utworzenie nowego obiektu
  const newItem = new ListItem(title);

  // utworzenie odpowiedniego id dla danego obiektu
  newItem.id = idNumber++;

  // dodanie obiektu do arrayOfListItems
  arrayOfListItems.push(newItem);

  return newItem;
}

// pobranie danych od użytkownika
function takeInputData() {
  let inputDescription = prompt('Dodaj nowe zadanie do listy');
  let newItem = createNewListItem(inputDescription);
  return newItem;
}

//aktualizacja interfejsu
function updateUI(newItem) {
  const list = document.getElementById('list');
  const li = document.createElement('li');
  list.appendChild(li);
  li.innerText = newItem.title;

  const deleteButton = document.createElement('button');
  deleteButton.setAttribute('class', 'delete');
  deleteButton.innerText = 'delete';

  const editButton = document.createElement('button');
  editButton.setAttribute('class', 'edit');
  editButton.innerText = 'edit';

  const doneButton = document.createElement('button');
  doneButton.setAttribute('class', 'done');
  doneButton.innerText = 'done';

  li.appendChild(deleteButton);
  li.appendChild(editButton);
  li.appendChild(doneButton);
}

//funkcja główna
function main() {
  getTodoList();
  handleEvents();
}

main();










































// let itemsArray = [];

// let Item = function(description) {
//   this.description = description;
// }

// let addNewItemToArray = function() {
//   let inputDescription = prompt('Please enter a new item');
//   let item = new Item(inputDescription);
//   itemsArray.push(item);
// }

// let displayUI = function() {
//   for(let i = 0; i < itemsArray.length; i++) {

//     console.log(itemsArray[i].description);

//     document.getElementById('list').innerHTML = '<li class="main-notes-list--style">' + itemsArray[i].description + '</li>';
//   }
// }


// const addButton = document.getElementById('btn1');
// addButton.addEventListener('click', function() {

//   addNewItemToArray();
//   displayUI();


// })





