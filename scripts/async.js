
function prepareList(response) {  
  const list = document.getElementById('list');
  response.forEach(element => {

    const li = document.createElement('li');
    list.appendChild(li);
    li.innerText = element.title;
    li.setAttribute('data-id', element.id);

    const deleteButton = document.createElement('button');
    deleteButton.setAttribute('class', 'delete');
    deleteButton.innerText = 'delete';

    const editButton = document.createElement('button');
    editButton.setAttribute('class', 'edit');
    editButton.innerText = 'edit';

    const doneButton = document.createElement('button');
    doneButton.setAttribute('class', 'done');
    doneButton.innerText = 'done';

    li.appendChild(deleteButton);
    li.appendChild(editButton);
    li.appendChild(doneButton);
  });
}

export function getTodoList() {   
  document.getElementById('list').innerHTML = '';                                                 
  fetch('http://195.181.210.249:3000/todo/').then((response) => {
    return response.json();
  }).then((response) => {
    prepareList(response);
  }).catch(() => {
    console.log('Error');
  });
}

export function postMethod(title) {               
  fetch('http://195.181.210.249:3000/todo/', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify({ title: title }),
  });
}

export function deleteMethod(id) {
  fetch('http://195.181.210.249:3000/todo/' + id, { // dodac id
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
  }).then(() => {
    getTodoList();
  });
}

export function putMethod(title,id) {
  fetch('http://195.181.210.249:3000/todo/' + id, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    body: JSON.stringify({ title: title }),
  });
}




